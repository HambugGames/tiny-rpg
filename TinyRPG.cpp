#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <ctime>

using namespace std;

const int MAP_WIDTH = 4; //Number of blocks wide
const int MAP_HEIGHT = 4; //Number of blocks high
const int blockSize = 8;//This is how big each block is

class Character
{
    public:
        Character(){
            //(1,1) is the top left corner of the map
            location_x = 1;
            location_y = 1;
            characterSprite = "@%";
            treasureCount = 0;
            isAlive = true;
        }

        int get_x(){return location_x;}
        int get_y(){return location_y;}
        int getTreasureCount(){return treasureCount;}
        std::string getSprite(){return characterSprite;}
        void attemptMove(std::string mapArr[MAP_HEIGHT*blockSize][MAP_WIDTH*blockSize], std::string input){
            bool moveSuccessful = false;
            int prev_x = location_x;
            int prev_y = location_y;
            //Check if coliding with walls. If not, move to new location.
            if(input == "w"){
                if(mapArr[location_y - 1][location_x] != "--"){
                    if(mapArr[location_y - 1][location_x] == "DD")
                        location_y -= 3;
                    else if(mapArr[location_y - 1][location_x] == "TT"){
                        location_y -= 1;
                        treasureCount += 1;
                    }
                    else if(mapArr[location_y - 1][location_x] == "MM"){//If we run into a monster, reset the game.
                        location_x = 1;
                        location_y = 1;
                        treasureCount = 0;
                        isAlive = false;
                    }
                    else
                        location_y -= 1;
                    moveSuccessful = true;
                }
            }
            else if(input == "s"){
                if(mapArr[location_y + 1][location_x] != "--"){
                    if(mapArr[location_y + 1][location_x] == "DD")
                        location_y += 3;
                    else if(mapArr[location_y + 1][location_x] == "TT"){
                        location_y += 1;
                        treasureCount += 1;
                    }
                    else if(mapArr[location_y + 1][location_x] == "MM"){//If we run into a monster, reset the game.
                        location_x = 1;
                        location_y = 1;
                        treasureCount = 0;
                        isAlive = false;
                    }
                    else
                        location_y += 1;
                    moveSuccessful = true;
                }
            }
            else if(input == "a"){
                if(mapArr[location_y][location_x - 1] != "|"){
                    if(mapArr[location_y][location_x - 1] == "D")
                        location_x -= 3;
                    else if(mapArr[location_y][location_x - 1] == "TT"){
                        location_x -= 1;
                        treasureCount += 1;
                    }
                    else if(mapArr[location_y][location_x - 1] == "MM"){//If we run into a monster, reset the game.
                        location_x = 1;
                        location_y = 1;
                        treasureCount = 0;
                        isAlive = false;
                    }
                    else
                        location_x -= 1;
                    moveSuccessful = true;
                }
            }
            else if(input == "d"){
                if(mapArr[location_y][location_x + 1] != "|"){
                    if(mapArr[location_y][location_x + 1] == "D")
                        location_x += 3;
                    else if(mapArr[location_y][location_x + 1] == "TT"){
                        location_x += 1;
                        treasureCount += 1;
                    }
                    else if(mapArr[location_y][location_x + 1] == "MM"){//If we run into a monster, reset the game.
                        location_x = 1;
                        location_y = 1;
                        treasureCount = 0;
                        isAlive = false;
                    }
                    else
                        location_x += 1;
                    moveSuccessful = true;
                }
            }

            if(moveSuccessful){
                mapArr[prev_y][prev_x] = "  ";
            }
        }
        bool getAlive(){return isAlive;}
        void setAlive(bool ali){isAlive = ali;}

    private:
        int location_x;
        int location_y;
        std::string characterSprite;
        int treasureCount;
        bool isAlive;
};

class VerticalWall{
   public:
    VerticalWall(){};

    std::string drawVerticalWallDefinite(){//This just draws a vertical wall.
        return "|";
    }
    std::string drawVerticalWallChance(){//This might draw a vert wall, or possibly a door, or whatever else we choose to add, instead.
        int randNum = 1 + (rand()%11);
        if(randNum < 10)
            return "|";
        else
            return "D";
    }
   private:

};

class HorizontalWall{
    public:
        HorizontalWall(){};

        std::string drawHorizontalWallDefinite(){//This just draws a horizontal wall.
            return "--";
        }
        std::string drawHorizontalWallChance(){//This might draw a horiz wall, or possibly a door, or whatever else we choose to add, instead.
            int randNum = 1 + (rand()%11);
            if(randNum < 10)
                return "--";
            else
                return "DD";
        }
   private:

};

class EmptySpace{
    public:
        EmptySpace(){};

        std::string drawEmptySpaceDefinite(){return "  ";}
        std::string drawEmptySpaceChance(){
            int randNum = 1 + (rand()%20);
            if(randNum <= 18)
                return "  ";
            else if(randNum <= 19)
                return "TT";
            else
                return "MM";
        }
    private:

};


class MapManager
{
    public:
        MapManager(){};

        void drawMap(){
            bool isOuterWall = true;
            for(int height = 1; height <= MAP_HEIGHT*blockSize; height++){
                for(int width = 1; width <= MAP_WIDTH*blockSize; width++){
                    //Determine if we're at an outer wall first. We can't draw nonwall things at outer walls
                    if(height == 1 or height == MAP_HEIGHT*blockSize
                        or width == 1 or width == MAP_WIDTH*blockSize){
                        isOuterWall = true;
                    }
                    else{
                        isOuterWall = false;
                    }

                    if(height%blockSize == 0 or height%blockSize == 1){//Draw horizontal walls if we're at a horizontal wall spot
                        if(isOuterWall){
                            if(width%blockSize == 0 or width%blockSize == 1)
                                cout << vertWall.drawVerticalWallDefinite();
                            else
                                cout << horWall.drawHorizontalWallDefinite();
                        }
                        else{
                            if(width%blockSize == 0 or width%blockSize == 1)
                                cout << vertWall.drawVerticalWallDefinite();//We don't want doors in unaccessable corners
                            else
                                cout << horWall.drawHorizontalWallChance();
                        }
                    }
                    else{//Else draw blank space
                        if(isOuterWall){
                            if(width%blockSize == 0 or width%blockSize == 1)
                                cout << vertWall.drawVerticalWallDefinite();
                            else
                                cout << "  ";//This will later be a draw-blank-space function of some sort
                        }
                        else{
                            if(width%blockSize == 0 or width%blockSize == 1)
                                cout << vertWall.drawVerticalWallChance();
                            else
                                cout << "  ";//This will later be a draw-blank-space function of some sort
                        }
                    }
                }
                cout << endl;
            }
        }
        void mapToArray(std::string mapArr[MAP_WIDTH*blockSize][MAP_HEIGHT*blockSize], Character ch){
            if(ch.getAlive() == false)//If character dies, resets map
                wasInitialized = false;
            bool isOuterWall = true;
            for(int height = 1; height <= MAP_HEIGHT*blockSize; height++){
                for(int width = 1; width <= MAP_WIDTH*blockSize; width++){
                    //Check if character location first
                    if((height - 1) == ch.get_y() && (width - 1) == ch.get_x()){
                        mapArr[height - 1][width - 1] = ch.getSprite();
                        continue;
                    }
                    if(wasInitialized)//Doesn't have to change array if we've already initialized it.
                        continue;
                    //Determine if we're at an outer wall first. We can't draw nonwall things at outer walls
                    if(height == 1 or height == MAP_HEIGHT*blockSize
                        or width == 1 or width == MAP_WIDTH*blockSize){
                        isOuterWall = true;
                    }
                    else{
                        isOuterWall = false;
                    }

                    if(height%blockSize == 0 or height%blockSize == 1){//Draw horizontal walls if we're at a horizontal wall spot
                        if(isOuterWall){
                            if(width%blockSize == 0 or width%blockSize == 1)
                                mapArr[height - 1][width - 1] = vertWall.drawVerticalWallDefinite();
                            else
                                mapArr[height - 1][width - 1] = horWall.drawHorizontalWallDefinite();
                        }
                        else{
                            if(width%blockSize == 0 or width%blockSize == 1)
                                mapArr[height - 1][width - 1] = vertWall.drawVerticalWallDefinite();//We don't want doors in unaccessable corners
                            else
                                mapArr[height - 1][width - 1] = horWall.drawHorizontalWallChance();
                        }
                    }
                    else{//Else draw blank space
                        if(isOuterWall){
                            if(width%blockSize == 0 or width%blockSize == 1)
                                mapArr[height - 1][width - 1] = vertWall.drawVerticalWallDefinite();
                            else
                                mapArr[height - 1][width - 1] =  empSpace.drawEmptySpaceDefinite();//This will later be a draw-blank-space function of some sort
                        }
                        else{
                            if(width%blockSize == 0 or width%blockSize == 1)
                                mapArr[height - 1][width - 1] = vertWall.drawVerticalWallChance();
                            else
                                mapArr[height - 1][width - 1] =  empSpace.drawEmptySpaceChance();//This will later be a draw-blank-space function of some sort
                        }
                    }
                }
            }
            wasInitialized = true;
        }
        void drawMapFromArray(string mapArr[MAP_WIDTH*blockSize][MAP_HEIGHT*blockSize]){
            for(int height = 0; height < MAP_HEIGHT*blockSize; height++){
                for(int width = 0; width < MAP_WIDTH*blockSize; width++){
                    cout << mapArr[height][width];
                }
                cout << endl;
            }
        }

        void setInitialized(bool init){wasInitialized = init;}//This function essentially resets the map
    private:
        VerticalWall vertWall;
        HorizontalWall horWall;
        EmptySpace empSpace;

        bool wasInitialized;//This determines if the map has been created yet, mostly for setting random variables only once
};

int main()
{
    srand((unsigned) time(0));

    MapManager mapMan;
    Character theCharacter;
    string theMap[MAP_HEIGHT*blockSize][MAP_WIDTH*blockSize];
    string input;

    while(input != "q"){
        cout << "Use w,a,s,d to move a direction. Then Press Enter.\n";
        cout << "Or press q then Enter to quit.\n";
        mapMan.mapToArray(theMap, theCharacter);
        mapMan.drawMapFromArray(theMap);
        cout << "Door: D or DD\t  Treasure: TT\t  Monster: MM\n";
        cout << "Current Treasure Count: " << theCharacter.getTreasureCount() << "\n";
        cout << "Input: ";
        cin >> input;
        theCharacter.setAlive(true);
        theCharacter.attemptMove(theMap, input);
        //system("clear");<--------------------Both of these perform the same function on Linux (as far as I'm aware).
        //cout << "\033[2J\033[1;1H";<-----------------------------|
    }

    return 0;
}